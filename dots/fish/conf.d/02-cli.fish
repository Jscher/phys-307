#!/usr/bin/env fish

## Exit if not Interactive ##
status is-interactive || exit

## Colorize grep and diff ##
alias grep 'grep --color=auto'
alias diff 'diff --color=auto'

## Get ip addr and Export With Symbol for Starship ##
set IP_ADDRESS (ip addr | \
                     sed -En "/inet.*enp24s0/s/ *inet ([0-9.]+)\/.*/\1/p")
export ICON_IP_ADDRESS="ﲾ $IP_ADDRESS"

## Colorize Bat ##
if test -e (command -v bat)
    alias bat 'bat --wrap=character -p   \
                            --force-colorization \
                            --theme Nord'
end


## Alias n to fasd directory
#alias n='fasd -sif'


# Interactive History Command
function __hist
    set -l argc (count $argv)
    if test $argc -eq 0
        history | sk | fish --no-config
    else
    test $argc -eq 1
    history | rg $argv | sk | fish --no-config
end

  end

alias hist __hist $argv

## Set default ls to exa ##
if command -v exa &>/dev/null
    export EXA_COLORS 'ln=36'
    alias ls '/usr/bin/exa --color=always -F --icons \
     	  	     			      --group-directories-first '
    alias lt 'ls -T -L3'
    alias la 'ls -a '
    alias ll 'ls -l'
    alias ll 'ls -la'
    alias ld 'lt -D'
end

## Alias Bluetooth ##
if command -v bluetoothctl &>/dev/null
    alias bt bluetoothctl
    alias shkz 'bt connect 20:74:CF:A6:B4:34 && \
                       pactl set-default-sink bluez_output.20_74_CF_A6_B4_34.1'
    alias sbar 'bt connect 40:16:3B:BD:B3:7D && \
                       pactl set-default-sink bluez_output.40_16_3B_BD_B3_7D.1'
end

## Alias shutdown, bios, and reboot ##
alias bios 'sudo systemctl reboot --firmware'
alias sdwn 'shutdown -P 0'
alias rb reboot


## ls after cd and clear ##
function __cd-ls --on-variable PWD
    pwd >/tmp/fish-cwd
    ls
end
alias clear '/usr/bin/clear;   ls'

## Alias rm and cp ##
alias rm='/usr/bin/rm -rf'
alias cp='/usr/bin/cp -r'

## Colorize man ##
set -x LESS_TERMCAP_us (printf "\e[04;38;5;146m")
set -x LESS_TERMCAP_md (printf "\e[01;38;5;74m")
set -x LESS_TERMCAP_so (printf "\033[01;44;33m")
set -x LESS_TERMCAP_mb (printf "\033[01;31m")
set -x LESS_TERMCAP_me (printf "\033[0m")
set -x LESS_TERMCAP_se (printf "\033[0m")
set -x LESS_TERMCAP_ue (printf "\e[0m")
set -l name (basename (status -f) .fish){_uninstall}

function $name --on-event $name
    set -e LESS_TERMCAP_md
    set -e LESS_TERMCAP_mb
    set -e LESS_TERMCAP_me
    set -e LESS_TERMCAP_ue
    set -e LESS_TERMCAP_us
    set -e LESS_TERMCAP_se
    set -e LESS_TERMCAP_so
end

function __hist
    set my_cmd (history | sk --height=75% --layout=reverse)

    #eval $my_cmd
    echo - cmd: "$my_cmd" >>$HOME/.local/share/fish/fish_history
    echo '  when:' $(date +%s) >>$HOME/.local/share/fish/fish_history
    test $(pwd) != $HOME && echo '  paths:' >>$HOME/.local/share/fish/fish_history && echo '    -' $HOME >>$HOME/.local/share/fish/fish_history
    eval $my_cmd && history merge

end

alias h __hist
