import numpy as np
import matplotlib.pyplot as plt

f,ax = plt.subplots(1)


### Time Parameters ###
dt        = 1   # Time Width
T_n       = [0] # Time Array


### Species A ###
A_0       = 10    # Initial A Mass
A_t_half  = 100   # A Half-Life
alpha     = np.log(2) / A_t_half  # A Decay Constant
A_rem     = .01   # A Cutoff
A_n       = [A_0] # A Population Array


### Species B ###
B_0       = 0     # Initial B Mass
B_t_half  = 100   # B Half-Life
beta      = np.log(2) / B_t_half  # B Decay Constant
B_rem     = .01   # B_Cutoff
B_n       = [B_0] # B Population Array


### Function to Update Population ###
def decay(A_n, B_n, T_n):

    A_n.append(A_n[-1]*(1 - alpha * dt))
    B_n.append(A_n[-1] * alpha + B_n[-1]*(1 - beta * dt))
    T_n.append(T_n[-1] + dt)

    print(T_n[-1], A_n[-1], B_n[-1])

    if A_n[-1] < A_rem or B_n[-1] < B_rem:

        return
    else:
        decay(A_n, B_n, T_n)


print(T_n[-1], A_n[-1], B_n[-1])

decay(A_n, B_n, T_n)

### Print Plots ###
plt.title('Mass v. Time', fontsize=18)
plt.xlabel('Time (yr)', fontsize=12)
plt.ylabel('Mass (kg)', fontsize=12)

plt.plot(T_n, A_n)
plt.plot(T_n, B_n)

ax.legend(['Uranium', 'Plutonium'])

ax.set_ylim(bottom=0)
ax.set_xlim(left=0)
plt.show()
