import sys, math, random
import numpy as np
import random
import matplotlib.pyplot as plt
fig, axs = plt.subplots(1)

sys.setrecursionlimit(10**9)

dt, dx, dy = [ .01, .01, .01 ]

c = 1
r = c * dt / dx

# From dx
n_L = int(1/dx)

# Or Count
n_L = 100
N = 84
N_mod = 14

# Set Length and intervals
L_arr = [ L * dx for L in range(0, n_L + 2) ]

## Gauss parameters ##
A = 1
alpha = .01
x_0 = 35

# Create Initial Plots
y_decay = 10

def main():

    def gauss_wave(n_L, x_0, A, alpha):
        y_0=[]
        for l in range(1, n_L + 1):
            y_0.append(A * np.exp(- alpha * ( l - x_0 )**2))
        return y_0

    def triangle_wave(n_L, x_0):
        y_0=[]
        for x in range(1, n_L + 1):
            if np.absolute( x / n_L - x_0 ) < .01:
                y = 1
            elif x /n_L >= x_0:
                y = - ( x / n_L - x_0)/(1 - x_0) + 1
            else:
                y = x/x_0 / n_L
            y_0.append(y)

        return y_0

    def propagate(y_0, y_1):
        y_2=[y_1[0]]

        for i in range(1, n_L+1):
            new_y = 2 * ( 1 - r**2 ) * y_1[i] - y_0[i] + r ** 2 * ( y_1[i+1] + y_1[i-1] )
            y_2.append(new_y)
        y_2.append(y_1[-1])

        return  y_1, y_2


    # plt.title('Wave on String', fontsize=18)
    # plt.xlabel('X', fontsize=12)
    # plt.ylabel('Y', fontsize=12)

    #y_0 = gauss_wave(n_L, x_0, A, alpha)
    y_0 = triangle_wave(n_L, .25)
    y_0.insert(0,0); y_0.append(0)
    y_1 = y_0


    for n in range(0,N):

        y_0, y_1 = propagate(y_0, y_1)
        if n % N_mod == 0:
            plt.plot(L_arr, y_1)

    axs.legend(['t_0', 't_1', 't_2', 't_3', 't_4', 't_5'])

    plt.show()

main()
