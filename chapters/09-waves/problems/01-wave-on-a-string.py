import sys, math, random, time
import numpy as np
import matplotlib.pyplot as plt

#fig, axs = plt.subplots(1)

dx = 0.01
dt = 0.01
L = 1
num_iterations = (int)(L/dx) + 1
line_arr = [0 for L in range(num_iterations)]
l_arr = [L * dx for L in range(num_iterations)]
T = 1
mu = 1

def gaussianwave(amp,alpha,P,L):
    y_arr=[]
    for i in range(num_iterations):
        y_arr.append(amp * np.exp(-(alpha*100) * (i*dx-P)**2))
    return y_arr

def trianglewave(amp,P,L):
    y_arr = []
    slope1 = (amp/P) * dx
    slope2 = - (amp/(L-P)) * dx
    #print(slope1, slope2)
    for i in range(num_iterations):
        print(i*dx, P)
        if i*dx < P:
            y_arr.append(i*slope1)
        else :
            y_arr.append((L/(L-P) + (i*slope2)))
    return y_arr

def squarewave(p1,p2,L):
    y_arr = []
    for i in range(num_iterations):
        if i*dx < p1:
            y_arr.append(0)
        elif i*dx > p1 and i*dx < p2:
            y_arr.append(1)
        else:
            y_arr.append(0)
    return y_arr

def updatewave(n_0,n_1,z):
    c = np.sqrt(T/mu)
    r = .5 * c * dt / dx
    y_arr = [0]
    #plt.clf()
    for i in range(1, num_iterations - 1):
        y_arr.append(2*(1-r**2)*n_1[i] - n_0[i] + r**2 * (n_1[i-1] + n_1[i+1]))
    #print(y_arr[50])
    y_arr.append(0)
    #if(z>125):
    #    return y_arr
#    plt.plot(l_arr, trianglewave(1,.5,L))
#    plt.plot(l_arr, y_arr)
#    plt.show()
    return y_arr#updatewave(n_1, y_arr, z+1)

sys.setrecursionlimit(10**9)
plt.ion()
fig = plt.figure()
ax = fig.add_subplot(111)
y_arr4 = updatewave(trianglewave(1,.25,L), trianglewave(1,.25,L),0)
ax.set_ylim(-2,2)
line, = ax.plot(l_arr, y_arr4)
pprev_yarr = trianglewave(1,.25,L)
prev_yarr = trianglewave(1,.25,L)
for z in range(1000000):
    y_arr4 = updatewave(pprev_yarr, prev_yarr,z)
    pprev_yarr = prev_yarr
    prev_yarr = y_arr4
    
    line.set_ydata(y_arr4)
    fig.canvas.draw()
    fig.canvas.flush_events()
    time.sleep(0.)
#plt.plot(l_arr, y_arr4)
plt.show()
