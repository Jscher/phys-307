import numpy as np
import sys
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button

fig, axs = plt.subplots(2)

sys.setrecursionlimit(10000)

### Constants ###
g   = 9.81
l   = g
dt  = 0.1
t_f = 60

### Initial conditions ###
t_n     = [0]
q = .5

Freq_N = np.sqrt( g / l )

theta_0 = 0.2
theta_n = [theta_0]

omega_0 = 0
omega_n = [omega_0]

### Driving Parameters ###
Freq_D   = .5
Force_D  = .5

def next_angular_values(t, theta, omega):

    delta_omega_grav =  g / l * np.sin(theta)
    delta_omega_damp = q * omega
    delta_omega_drive = Force_D * np.sin(Freq_D * t)
    delta_omega =  delta_omega_grav + delta_omega_damp + delta_omega_drive

    omega = omega - delta_omega * dt

    theta = theta + omega * dt
    if theta > np.pi:
        theta = theta - 2 * np.pi
    if theta <  - np.pi:
        theta =  theta + 2 * np.pi

    return theta, omega


def calc_next_step(t_n, theta_n, omega_n):
    t_n.append(t_n[-1] + dt )

    theta, omega = next_angular_values(t_n[-1], theta_n[-1], omega_n[-1])

    theta_n.append(theta)
    omega_n.append(omega)

    return True if t_n[-1] > t_f else calc_next_step(t_n, theta_n, omega_n)

calc_next_step(t_n, theta_n, omega_n)

fig.suptitle('Chaotic Harmonic Motion', fontsize=18)
axs[0].plot(t_n, theta_n)

axs[0].set_xlabel('Time (s)', fontsize=12)
axs[0].set_ylabel('Angle (Rad)', fontsize=12)

axs[1].scatter(theta_n, omega_n)

axs[1].set_ylabel('Omega (Rad/s)', fontsize=12)
axs[1].set_xlabel('Theta (Rad)', fontsize=12)

plt.show()
