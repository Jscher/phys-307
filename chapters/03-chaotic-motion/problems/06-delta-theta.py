import numpy as np
import sys
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button

fig, axs = plt.subplots(1)

sys.setrecursionlimit(10000)

### Constants ###
g   = 9.81
dt  = 0.01
t_f = 60

### Initial conditions ###
t_n = [0]
q_1 = 0.5
q_2 = q_1

### Pendulum Parameters ###
l_1   = g
l_2   = l_1

omega_1 = 0
omega_2 = omega_1

<<<<<<< HEAD
omega_1_0 = 0.3
omega_1_n = [omega_1_0]

omega_2_0 = 0.29
omega_2_n = [omega_2_0]

delta_theta_n = [0]
=======
### Delta Theta Variables ###
delta_theta_0 = .01
delta_theta_n = [ delta_theta_0 ]
theta_1 = 0.5
theta_2 = theta_1 + delta_theta_0
>>>>>>> 77a7e45ebbe5617641dac4b0b8d516c1f3c05082

### Driving Parameters ###
Freq_D_1   = .25
Freq_D_2   = Freq_D_1
Force_D_1  = 1.2
Force_D_2  = Force_D_1

def next_angular_values(t, theta, omega, l, Force_D, Freq_D, q):
    delta_omega_grav =  g / l * np.sin(theta)
    delta_omega_damp = q * omega
    delta_omega_drive = Force_D * np.sin(Freq_D * t)
    delta_omega =  delta_omega_grav + delta_omega_damp + delta_omega_drive

    omega = omega - delta_omega * dt

    theta = theta + omega * dt
    if theta > np.pi:
        theta = theta - 2 * np.pi
    if theta <  - np.pi:
        theta =  theta + 2 * np.pi

    return theta, omega

def calc_next_step(t_n, delta_theta_n, theta_1, theta_2, omega_1, omega_2):
    t_n.append(t_n[-1] + dt )

    theta_1, omega_1 = next_angular_values(t_n[-1], theta_1, omega_1, l_1, Force_D_1, Freq_D_1, q_1)
    theta_2, omega_2 = next_angular_values(t_n[-1], theta_2, omega_2, l_2, Force_D_2, Freq_D_2, q_2)

    print(theta_2, omega_2)

<<<<<<< HEAD
    omega_1_n.append(omega_1_n[-1] - delta_omega_1 * dt)
    omega_2_n.append(omega_2_n[-1] - delta_omega_2 * dt)

    theta_1_new = theta_1_n[-1] + omega_1_n[-1] * dt
    if theta_1_new > np.pi:
        theta_1_new = theta_1_new - 2 * np.pi
    if theta_1_new <  - np.pi:
        theta_1_new =  theta_1_new + 2 * np.pi
    theta_1_n.append(theta_1_new)

    theta_2_new = theta_2_n[-1] + omega_2_n[-1] * dt
    if theta_2_new > np.pi:
        theta_2_new = theta_2_new - 2 * np.pi
    if theta_2_new <  - np.pi:
        theta_2_new =  theta_2_new + 2 * np.pi
    theta_2_n.append(theta_2_new) 

    delta_theta_n.append(theta_2_new - theta_1_new)
    if delta_theta_n[-1] > np.pi:
        delta_theta_n[-1] = delta_theta_n[-1] - 2 * np.pi
    if delta_theta_n[-1] <  - np.pi:
        delta_theta_n[-1] =  delta_theta_n[-1] + 2 * np.pi
=======
    delta_theta_n.append((theta_2 - theta_1))

    return True if t_n[-1] > t_f else calc_next_step(t_n, delta_theta_n, theta_1, theta_2, omega_1, omega_2)

calc_next_step(t_n, delta_theta_n, theta_1, theta_2, omega_1, omega_2)
>>>>>>> 77a7e45ebbe5617641dac4b0b8d516c1f3c05082

fig.suptitle('Delta Theta = %1.1f degrees' % np.rad2deg(delta_theta_0), fontsize=18)
plt.plot(t_n, delta_theta_n)

plt.yscale('log')
plt.xlabel('Time (s)', fontsize=12)
plt.ylabel('Angle (Rad)', fontsize=12)

plt.show()
