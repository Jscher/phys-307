To simulate orbital motion, we begin with Newton\'s Universal Law of
Gravitation:

$$|F_G| = G \frac{m M }{r^2}$$

Applying Newton\'s 2nd Law, considering the motion of the smaller
object, $m$, in Cartesian Coordinates:

$$\frac{d^2x}{dt^2} = \frac{F_{G,x}}{m} \qquad \frac{d^2y}{dt^2} = \frac{F_{G,y}}{m}$$

![](./images/01-earth-sun-trig.png)

Where:

$$F_{G,x} = - G \frac{m M }{r^2}\cos\theta = - G \frac{ m M }{r^2}\cdot \frac{x}{r} $$
$$F_{G,y} = - G \frac{m M }{r^2}\sin\theta = - G \frac{ m M }{r^2}\cdot \frac{y}{r} $$

As previously, these equations can be cast in terms of first order
derivatives so that:

$$ \frac{dv_x}{dt} = - G \frac{Mx}{r^3}; \qquad \frac{dx}{dt} = v_x$$
$$ \frac{dv_y}{dt} = - G \frac{My}{r^3}; \qquad \frac{dy}{dt} = v_y$$

Given the scale of mass, distance, and time being many orders of
magnitude in SI units, it is more convenient to work in astronomical
units where 1 AU is the average distance between the Earth and Sun, and
in years rather than seconds.

To convert mass, we consider the centripetal force between the Earth and
the Sun as the Earth\'s orbit around the Sun is roughly circular:

$$\frac{M_E v^2}{r} =F_G = G\frac{M_S M_E}{r^2}$$

Rearranging for $GM_S$:

$$FM_S = v^2 r = v^2 r = 4\pi^2 AU^3 /yr^2$$

Following the process we have previously with the Euler-Cromer Method:

$$v_{x,i+1} = v_{x,i} -\frac{4\pi^2 x_i}{r_i^3}\Delta t$$
`\vspace{.05cm}`{=latex} $$x_{i+1} =x_i + v_{x,i+1}\Delta t$$
`\vspace{.05cm}`{=latex}
$$v_{y,i+1} = v_{y,i} -\frac{4\pi^2 y_i}{r_i^3}\Delta t$$
`\vspace{.05cm}`{=latex}
$$y_{i+1} =y_i + v_{y,i+1}\Delta t$$`\vspace{.05cm}`{=latex}
