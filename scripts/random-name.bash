#!/usr/bin/env bash

#REMOVED_NAMES=( 'Jasper' )
NAMES=( 'Albert' 'Anas'  'Kairos' 'Kenny' 'William' )


NUM=$(( (RANDOM % 50) + 10 ))

for (( i = 0;  i<"$NUM"; ++i )); do
	NAME=$(echo "${NAMES[@]}" | tr ' ' '\n' | shuf | head -1)
	echo "$i: $NAME"
	sleep 0.75
done

clear; echo "======>> $NAME <<======"


