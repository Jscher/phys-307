#!/usr/bin/env bash

SCRIPT_SOURCE=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "$SCRIPT_SOURCE" || exit

DOT_CONFIG_PATH="$HOME/.config"

PACKAGES=( 'git' 'font' 'alacritty' 'eza' 'fish' 'gstreamer' 'neovim' 'starship' 'tealdeer' 'tmux' 'vscodium-bin' )

FONTS=( 'ttf-fantasque-nerd' 'ttf-fantasque-sans-mono' 'ttf-liberation' 'ttf-nerd-fonts-symbols' 'ttf-nerd-fonts-symbols-common' 'ttf-nerd-fonts-symbols-mono' )
DOTS=( 'alacritty' 'fish' 'nvim' )

function _install_yay {
    git clone https://aur.archlinux.org/yay.git
    cd yay || exit; makepkg -si --noconfirm
    cd ../; rm -rf yay
}

main() {

    command -v || _install_yay

    for FONT in "${FONTS[@]}"; do
    	command -v $FONT || yes '' | yay --noconfirm $FONT
	done

    for PACKAGE in "${PACKAGES[@]}"; do
    	command -v $PACKAGE || yes '' | yay --noconfirm $FONT
	done

	for DOT in "${DOTS[@]}"; do
		if [ -e "$DOT_CONFIG_PATH/$DOT" ]; then
			mv  "$DOT_CONFIG_PATH/$DOT"  "$DOT_CONFIG_PATH/$DOT-$(date +%s)"
		fi
			cp "../dots/$DOT" "$DOT_CONFIG_PATH/$DOT"
	done

	if [ -e "$HOME/.ssh/config" ]; then
		mv "$HOME/.ssh/config"  "$HOME/.ssh/config-$(date +%s)"
	fi
		cp "../dots/ssh-config" "$HOME/.ssh/config"
}

main

